package sondow.markov;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class BotConfigFactory {

    private Environment environment;

    public BotConfigFactory(Environment environment) {
        this.environment = environment;
    }

    public BotConfigFactory() {
        this(new Environment());
    }

    public BotConfig configure() {

        Environment environment = new Environment();

        // Get the source twitter account and target twitter account from environment variables.
        // Pick a random one. The expected format has pairs as source, then a hyphen, then the
        // target, with pairs separated by commas:
        // joesondow-joeebooks,rikergoogling-rikerebooks,picardtips-picardebooks
        String sourceTargetAccountPairsString = environment.get("twitter_account_pairs_csv");
        if (sourceTargetAccountPairsString == null) {
            throw new RuntimeException("Nothing set in env var twitter_account_pairs_csv");
        }
        List<String> srcTgtAcctPairs = Arrays.asList(sourceTargetAccountPairsString.split(","));
        int accountCount = srcTgtAcctPairs.size();
        int nowHour = new GregorianCalendar(TimeZone.getTimeZone("UTC")).get(Calendar.HOUR_OF_DAY);
        int remainder = nowHour % accountCount;

        // If nowHour is 12, accountCount is 3, remainder is 0.
        // If nowHour is 11, accountCount is 3, remainder is 2.
        // If nowHour is 10, accountCount is 3, remainder is 1.
        // If nowHour is 9, accountCount is 3, remainder is 0.
        // Refactor and unit test this
        String sourceTargetPairString = srcTgtAcctPairs.get(remainder);
        System.out.println("sourceTargetPairString: " + sourceTargetPairString + ", accountCount: "
                + accountCount + ", nowHour: " + nowHour + ", remainder: " + remainder);
        List<String> sourceAndTargetAccount = Arrays.asList(sourceTargetPairString.split("-"));
        String source = sourceAndTargetAccount.get(0);
        String trgt = sourceAndTargetAccount.get(1);

        String fileName = source.toLowerCase() + "markov.json";
        String message = new TweetGenerator(new Random()).loadAndGenerate(fileName);
        if (message == null || message.isEmpty()) {
            throw new RuntimeException("What up with the empty message?");
        }
        Configuration twitterConf = configureTwitter(trgt);
        return new BotConfig(twitterConf, message);
    }

    /**
     * @return configuration containing Twitter authentication strings and other variables
     */
    private Configuration configureTwitter(String target) {
        ConfigurationBuilder configBuilder = new ConfigurationBuilder();

        String credentialsCsv = environment.get("cred_twitter_" + target);

        String[] tokens = credentialsCsv.split(",");
        String screenName = tokens[0];
        String consumerKey = tokens[1];
        String consumerSecret = tokens[2];
        String accessToken = tokens[3];
        String accessTokenSecret = tokens[4];

        configBuilder.setUser(screenName);
        configBuilder.setOAuthConsumerKey(consumerKey);
        configBuilder.setOAuthConsumerSecret(consumerSecret);
        configBuilder.setOAuthAccessToken(accessToken);
        configBuilder.setOAuthAccessTokenSecret(accessTokenSecret);

        return configBuilder.setTrimUserEnabled(true).setTweetModeExtended(true).build();
    }
}
