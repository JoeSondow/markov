package sondow.markov;

import twitter4j.conf.Configuration;

public class BotConfig {

    private Configuration twitterConfig;
    private String message;

    BotConfig(Configuration twitterConfig, String message) {
        this.twitterConfig = twitterConfig;
        this.message = message;
    }

    public Configuration getTwitterConfig() {
        return twitterConfig;
    }

    public String getMessage() {
        return message;
    }
}
