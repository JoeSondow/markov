package sondow.markov;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class Bot {

    private BotConfig botConfig;

    public Bot(BotConfig botConfig) {
        this.botConfig = botConfig;
    }

    public Bot() {
        this(new BotConfigFactory().configure());
    }

    public void go() {
        Tweeter tweeter = new Tweeter(botConfig.getTwitterConfig());
        tweeter.tweet(botConfig.getMessage());
    }
}
